<?php

namespace ApiBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class TaskData
 * @package ApiBundle\Entity
 */
class TaskData
{
    /**
     * @var int
     *
     * @Assert\Type("integer")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    private $content;

    /**
     * @var string
     *
     * @Assert\NotNull()
     * @Assert\Type("bool")
     */
    private $completed;

    /**
     * Set id.
     *
     * @param int $id
     *
     * @return self
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set content.
     *
     * @param string $content
     *
     * @return self
     */
    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content.
     *
     * @return string
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * Set completed.
     *
     * @param bool $completed
     *
     * @return self
     */
    public function setCompleted(bool $completed): self
    {
        $this->completed = $completed;

        return $this;
    }

    /**
     * Get completed.
     *
     * @return bool
     */
    public function getCompleted(): ?bool
    {
        return $this->completed;
    }
}
