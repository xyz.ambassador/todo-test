<?php

namespace ApiBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Gedmo\Mapping\Annotation as Gedmo;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\UserRepository")
 * @ORM\Cache(usage="READ_ONLY")
 *
 *
 * @JMS\ExclusionPolicy("ALL")
 */
class User extends BaseUser
{
    const DEFAULT_NAME = 'root';

    const DEFAULT_PASSWORD = 'root';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @JMS\Expose
     * @JMS\Groups({"api"})
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255)
     *
     * @JMS\Expose
     * @JMS\Groups({"api"})
     */
    protected $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="text")
     *
     * @JMS\Expose
     * @JMS\Groups({"api"})
     *
     */
    protected $password;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @var Task[]|Collection
     *
     * @ORM\OneToMany(targetEntity="ApiBundle\Entity\Task", mappedBy="user")
     *
     * @JMS\Expose
     * @JMS\Groups({"api"})
     * @JMS\Accessor(getter="getTasksCount")
     * @JMS\Type("integer")
     */
    private $tasks;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username.
     *
     * @param string $username
     *
     * @return self
     */
    public function setUserName($username)
    {
        $this->username = $username;

        parent::setUsername($username);
        parent::setEmail($username);

        return $this;
    }

    /**
     * Get username.
     *
     * @return string
     */
    public function getUserName()
    {
        return $this->username;
    }

    /**
     * Set password.
     *
     * @param string $password
     *
     * @return self
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password.
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return self
     */
    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return self
     */
    public function setUpdatedAt(\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    /**
     * Add task.
     *
     * @param Task $task
     *
     * @return self
     */
    public function addTask(Task $task): self
    {
        $this->tasks[] = $task;

        return $this;
    }

    /**
     * Remove task.
     *
     * @param Task $post
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePost(Task $post): bool
    {
        return $this->tasks->removeElement($post);
    }

    /**
     * Get tasks.
     *
     * @return Task[]|Collection
     */
    public function getTasks(): Collection
    {
        return $this->tasks;
    }

    /**
     * @return int
     */
    public function getTasksCount(): int
    {
        return $this->tasks->count();
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return true;
    }
}
