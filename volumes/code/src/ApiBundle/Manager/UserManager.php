<?php declare(strict_types = 1);

namespace ApiBundle\Manager;

use FOS\UserBundle\Doctrine\UserManager as FOSUserManager;

/**
 * Class UserManager
 * @package ApiBundle\Manager
 */
class UserManager extends FOSUserManager
{
    /**
     * {@inheritdoc}
     */
    public function findUserByEmail($email)
    {
        return $this->findUserBy(array('username' =>$email));
    }

    /**
     * {@inheritdoc}
     */
    public function findUserByUsername($username)
    {
        return $this->findUserBy(array('username' => $username));
    }
}