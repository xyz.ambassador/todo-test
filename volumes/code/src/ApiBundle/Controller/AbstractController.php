<?php declare(strict_types = 1);

namespace ApiBundle\Controller;

use ApiBundle\Entity\ApiResponse;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Context\Context;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class AbstractApiController
 * @package ApiBundle\Controller
 */
abstract class AbstractController extends FOSRestController
{
    /**
     * @param string  $formType
     * @param Request $request
     * @param array   $additional
     * @return mixed
     * @throws HttpException
     */
    protected function getFormData(string $formType, Request $request, array $additional = [])
    {
        $request->request->add($additional);
        $form = $this->createForm($formType);
        $form->submit($request->request->all());

        if (!$form->isSubmitted() || !$form->isValid()) {
            throw new HttpException(
                Response::HTTP_BAD_REQUEST,
                (string) $form->getErrors(true, false)
            );
        }

        return $form->getData();
    }

    /**
     * @param ApiResponse $response
     * @param array       $groups
     * @return Response
     */
    protected function response(ApiResponse $response, array $groups = [])
    {
        $code = $response->getErrorCode() ?? Response::HTTP_OK;

        $view = $this->view($response, $code);

        $groups[] = ApiResponse::JMS_DEFAULT_GROUP;
        $context = new Context();
        $context->setGroups($groups);
        $view->setContext($context);

        return $this->handleView($view);
    }

}
