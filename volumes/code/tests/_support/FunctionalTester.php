<?php

use Symfony\Bundle\FrameworkBundle\Routing\Router;

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = null)
 *
 * @SuppressWarnings(PHPMD)
 */
class FunctionalTester extends \Codeception\Actor
{
    use _generated\FunctionalTesterActions;

    /**
     * @param string $username
     * @param string $password
     * @return array
     */
    public function amAuthenticate(string $username, string $password)
    {
        $this->wantTo('authenticate a user');

        $this->haveHttpHeader('Content-Type', 'application/json');

        $uri = $this->generateUrl('api_login_check');

        $this->sendPOST($uri, [
            'username' => $username,
            'password' => $password,
        ]);

        $this->seeResponseCodeIs(200);

        $this->seeResponseIsJson();

        $token = $this->grabDataFromResponseByJsonPath('$.token');

        $this->deleteHeader('Authorization');

        $this->amBearerAuthenticated($token[0]);

        return $token;
    }

    /**
     * @param string $name
     * @param array  $parameters
     * @param int    $referenceType
     * @return mixed
     */
    public function generateUrl(
        string $name,
        array $parameters = [],
        int $referenceType = Router::ABSOLUTE_PATH
    ) {
        /** @var Router $router */
        $router = $this->grabService('router');

        return $router->generate($name, $parameters, $referenceType);
    }
}
